import UserStore from "../store/user";
import { getModule } from "nuxt-property-decorator";
import { Middleware } from "@nuxt/types";

const refreshUser: Middleware = (context) => {
    if (process.client) {
        if (context.$auth.loggedIn) {
            const id = context.$auth.$storage.getUniversal('userId');
            const user = getModule(UserStore, context.store)
            user.getUSer(id).then((res) => {
                context.$auth.setUser(res)
            }).catch((error) => {
                console.error('Ocurrio un error:', error)
            })
        }
    }
}

export default refreshUser;