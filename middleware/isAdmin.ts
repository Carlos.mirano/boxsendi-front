import { Middleware } from "@nuxt/types";

const isAdmin: Middleware = (context) => {
    if (context.$auth.loggedIn) {
        if (context.$auth.user && context.$auth.user.admin) {
            console.log('continue')
        } else {
            return context.redirect('/inicio')
        }
    } else {
        return context.redirect('/login')
    }
}

export default isAdmin;