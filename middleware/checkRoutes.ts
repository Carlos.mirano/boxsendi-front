import { Middleware } from "@nuxt/types";
import { routesExclude } from "../utils/routesExclude.util";

const checkRoutes: Middleware = (context) => {
    if (context.$auth.loggedIn) {
        if (routesExclude.includes(context.route.fullPath)) {
            context.redirect('/inicio')
        }
    }
}

export default checkRoutes