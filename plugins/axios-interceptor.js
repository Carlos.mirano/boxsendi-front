export default function ({ $axios, store }) {
    if (process.browser) {
        $axios.interceptors.request.use(
            function (config) {
                let token = localStorage.getItem('auth._token.local')
                if (token) {
                    config.headers.Authorization = `${token}`
                }
                return config
            },
            function (error) {
                return Promise.reject(error)
            }
        )

        $axios.interceptors.response.use(
            function (response) {
                return response
            },
            function (error) {
                if (
                    error.response.status === 401 &&
                    error.response.statusText === 'UNAUTHORIZED'
                ) {
                    store.dispatch('authentication/logOut')
                }
                return Promise.reject(error)
            }
        )
    }
}
