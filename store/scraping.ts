import {
    Module,
    VuexModule,
    VuexMutation,
    VuexAction,
} from "nuxt-property-decorator";
import { $axios } from "@/utils/api";

@Module({
    name: 'scraping',
    stateFactory: true,
    namespaced: true,
})
export default class Scraping extends VuexModule {

    scrap: any = {
        descripcion: '',
        moneda: '',
        precio: 0.00,
        imagen: '',
        asin: ''
    }

    get Scrap() {
        return this.scrap
    }

    @VuexMutation
    setScrap(scrap: any) {
        this.scrap = scrap
    }

    @VuexAction({
        rawError: true,
        commit: 'setScrap'
    })
    async getScrap(payload: any) {
        const v = await $axios.$post(`${$axios.defaults.baseURL}/api/scraping`, payload)
        console.log("v", v)
        return v.product
    }

    @VuexAction({
        commit: 'setScrap',
      })
      limpiarStore() {
        return {
          descripcion: '',
          moneda: '',
          precio: 0.0,
          imagen: '',
          asin: '',
        }
      }
}