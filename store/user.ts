import {
    Module,
    VuexModule,
    VuexMutation,
    VuexAction,
} from "nuxt-property-decorator";
import { $axios } from "@/utils/api";

@Module({
    name: 'user',
    stateFactory: true,
    namespaced: true
})
export default class User extends VuexModule {
    user: any = {}
    users: any = []

    get oneUser() {
        return this.user
    }

    get allUsers() {
        return this.users
    }

    @VuexMutation
    setUser(user: any) {
        this.user = user
    }

    @VuexMutation
    setAllUsers(users: any) {
        this.users = users
    }

    @VuexAction({
        rawError: true,
        commit: 'setUser'
    })
    async getUSer(payload: any) {
        const res = await $axios.$get(`${$axios.defaults.baseURL}/api/usuarios/${payload}`)
        return res.user
    }

    @VuexAction({
        rawError: true,
        commit: 'setAllUsers'
    })
    async getAllUsers() {
        const res = await $axios.$get(`${$axios.defaults.baseURL}/api/usuarios`)
        return res.usuarios
    }
}