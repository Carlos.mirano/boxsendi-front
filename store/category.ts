import { Module, VuexModule, VuexAction, VuexMutation } from "nuxt-property-decorator";
import { $axios } from "@/utils/api";

@Module({
    name: 'category',
    stateFactory: true,
    namespaced: true,
})
export default class Category extends VuexModule {
    category: any = [];

    get listCategory(): Array<any> {
        return this.category
    }

    @VuexMutation
    setCategory(category: any) {
        this.category = category
    }

    @VuexAction({
        rawError: true,
        commit: 'setCategory'
    })
    async getCategories() {
        const resp: any = await $axios.$get('/api/categoria')
        return resp.categorias
    }

    @VuexAction({
        rawError: true
    })
    async createCategory(payload: any) {
        try {
            const resp = await $axios.$post('/api/categoria', payload)
            return resp
        } catch (error: any) {
            return error.response;
        }
    }

    @VuexAction({
        rawError: true
    })
    async updateCategory(payload: any) {
        try {
            const resp = await $axios.$put('/api/categoria', payload)
            return resp
        } catch (error) {
            return error
        }
    }

    @VuexAction({
        rawError: true
    })
    async deleteCategory(payload: any) {
        try {
            const resp = await $axios.$delete('/api/categoria', { data: payload })
            return resp
        } catch (error) {
            return error
        }
    }
}