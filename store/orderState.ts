import { VuexAction, VuexMutation } from "nuxt-property-decorator"
import { Module, VuexModule } from "vuex-module-decorators"
import { $axios } from "~/utils/api"

@Module({
    name: 'orderState',
    stateFactory: true,
    namespaced: true
})
export default class OrderState extends VuexModule {
    orderState: any = []

    get listOrderStateGetter(): Array<any> {
        return this.orderState
    }

    @VuexMutation
    setOrderState(payload: any) {
        this.orderState = payload
    }

    @VuexAction({
        rawError: true,
        commit: 'setOrderState'
    })
    async getOrderState() {
        const resp: any = await $axios.$get('/api/orderstate')
        return resp.orderState
    }
}