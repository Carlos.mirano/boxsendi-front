import { VuexAction } from "nuxt-property-decorator";
import { Module, VuexModule } from "vuex-module-decorators";
import { $axios } from "~/utils/api";

@Module({
    name: 'register',
    stateFactory: true,
    namespaced: true
})
export default class Register extends VuexModule {
    @VuexAction({
        rawError: true,
    })
    async registerAction(payload: any) {
        const resp: any = await $axios.$post('/api/usuarios', payload)
    }
}