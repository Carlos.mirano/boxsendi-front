import { VuexAction, VuexMutation } from "nuxt-property-decorator";
import { Module, VuexModule } from "vuex-module-decorators";
import { $axios } from "~/utils/api";

@Module({
    name: 'orders',
    stateFactory: true,
    namespaced: true,
})
export default class Orders extends VuexModule {
    allOrders: any = [];

    /**
     * Getter
     */
    get allOrdersGetter(): Array<any> {
        return this.allOrders;
    }

    @VuexMutation
    setAllOrders(payload: any) {
        this.allOrders = payload
    }

    @VuexAction({
        rawError: true,
        commit: 'setAllOrders'
    })
    async getAllOrders() {
        try {
            const resp: any = await $axios.$get(`/api/cotizaciones/`)
            return resp.quotation
        } catch (error) {
            return error
        }
    }
}