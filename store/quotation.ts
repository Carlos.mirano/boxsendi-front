import {
    Module,
    VuexModule,
    VuexMutation,
    VuexAction,
} from "nuxt-property-decorator";
import { $axios } from "@/utils/api";

@Module({
    name: 'quotation',
    stateFactory: true,
    namespaced: true,
})
export default class Quotation extends VuexModule {
    @VuexAction({
        rawError: true
    })
    async createQuotation(payload: any) {
        try {
            const resp: any = await $axios.$post(`${$axios.defaults.baseURL}/api/cotizaciones`, payload)
            return resp
        } catch (error) {
            return error
        }
    }
}