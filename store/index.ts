// import Vuex from 'vuex'
//import { initialiseStores } from '~/utils/store-accessor'

//const initializer = (store: Store<any>) => initialiseStores(store)

//export const plugins = [initializer]
//export * from '~/utils/store-accessor'

// export const store = new Vuex.Store<any>({})

import Vuex from "vuex";
import Scraping from "@/store/scraping";

export function createStore() {
    return new Vuex.Store({
        modules: {
            Scraping
        }
    })
}