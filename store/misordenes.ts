import {
    Module,
    VuexModule,
    VuexMutation,
    VuexAction,
} from "nuxt-property-decorator";
import { $axios } from "@/utils/api";

@Module({
    name: 'misordenes',
    stateFactory: true,
    namespaced: true,
})
export default class MisOrdenes extends VuexModule {
    misOrdenes: any = []

    get listMisOrdenes(): Array<any> {
        return this.misOrdenes
    }

    @VuexMutation
    setMisOrdenes(misOrdenes: any) {
        this.misOrdenes = misOrdenes
    }

    @VuexAction({
        rawError: true,
        commit: 'setMisOrdenes'
    })
    async getMisOrdenes(payload: any) {
        const resp: any = await $axios.$get(`/api/cotizaciones/${payload}`)
        return resp.cotizaciones
    }

    @VuexAction({
        rawError: true
    })
    async updateQuotation(payload: any) {
        const resp: any = await $axios.$put(`/api/cotizaciones/${payload.id}`, payload)
        return resp
    }

    @VuexAction({
        rawError: true
    })
    async deleteQuotation(payload: any) {
        console.log('payload', payload)
        const resp: any = await $axios.$delete(`/api/cotizaciones/${payload.id}/${payload.estado}`)
        return resp
    }
}